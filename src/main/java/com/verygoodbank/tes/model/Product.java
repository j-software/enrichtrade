package com.verygoodbank.tes.model;


import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class Product {
    @CsvBindByName(column = "product_id")
    private Integer id;

    @CsvBindByName(column = "product_name")
    private String name;
}