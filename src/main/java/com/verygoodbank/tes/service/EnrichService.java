package com.verygoodbank.tes.service;


import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.verygoodbank.tes.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class EnrichService {

    @Value("${product.definition.file}")
    private String productDefPath;

    private Map<String, String> productData = null;

    private static String LINE_SEPARATOR = "\r\n";
    private static String DATA_SEPARATOR = ",";
    private static String MISSING_NAME = "Missing Product Name";
    private static SimpleDateFormat df = new SimpleDateFormat("YYYYMMDD");
    private static Logger LOG = LoggerFactory.getLogger(EnrichService.class);

    public String enrichTradeData(MultipartFile tradeData) throws IOException {
        String line;
        InputStream is = tradeData.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("date,product_name,currency,price" + LINE_SEPARATOR);
        boolean firstRow = true;
        while ((line = br.readLine()) != null) {
            if (firstRow) {
                firstRow = false;
            } else {
                processSingleTradeLine(line, strBuilder);
            }
        }
        return strBuilder.toString();
    }

    public void processSingleTradeLine(String line, StringBuilder strBuilderResult) {
        String[] lineArray = line.split(DATA_SEPARATOR);
        try {
            validateData(lineArray[0]);
            strBuilderResult.append(lineArray[0] + DATA_SEPARATOR +
                translateProductId(lineArray[1]) + DATA_SEPARATOR +
                lineArray[2] + DATA_SEPARATOR +
                lineArray[3] + LINE_SEPARATOR);
        } catch (ParseException e) {
            LOG.error("Error parsing date on row {}", line);
        }
    }

    public String translateProductId(String productId) {
        if (productData != null && productData.get(productId) != null) {
            return productData.get(productId);
        } else {
            return MISSING_NAME;
        }
    }

    private void validateData(String data) throws ParseException {
        df.parse(data);
    }


    @EventListener(ApplicationReadyEvent.class)
    private void loadProductDataFromResource() throws IOException {
        if (productData == null) {
            productData = new HashMap<>();
            InputStream resource = new ClassPathResource(productDefPath).getInputStream();
            Reader reader = new BufferedReader(new InputStreamReader(resource));
            CsvToBean<Product> csvReader = new CsvToBeanBuilder(reader)
                .withType(Product.class)
                .withSeparator(',')
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .build();
            csvReader.parse().stream().forEach(p -> productData.put("" + p.getId(), p.getName()));
        }

    }
}
