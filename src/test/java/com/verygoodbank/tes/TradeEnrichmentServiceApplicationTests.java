package com.verygoodbank.tes;

import com.verygoodbank.tes.service.EnrichService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class TradeEnrichmentServiceApplicationTests {

    @Spy
    EnrichService enrichService;

    private static String LINE_SEPARATOR = "\r\n";

    @Test
    void testCorrectTradeLine() {

        //given
        String tradeDataLine = "20160101,1,EUR,10.0";
        StringBuilder strBuilder = new StringBuilder();

        //when
        doReturn("Treasury Bills Domestic").when(enrichService).translateProductId(eq("1"));
        enrichService.processSingleTradeLine(tradeDataLine, strBuilder);

        //then
        assertEquals(strBuilder.toString(), "20160101,Treasury Bills Domestic,EUR,10.0" + LINE_SEPARATOR);
    }

    @Test
    void testIncorrectDateTradeLine() {

        //given
        String tradeDataLine = "2016/01/01,1,EUR,10.0";
        StringBuilder strBuilder = new StringBuilder();

        //when
        enrichService.processSingleTradeLine(tradeDataLine, strBuilder);

        //then
        assertEquals(strBuilder.toString(), "");
    }

    @Test
    void testMissingProductMapping() {

        //given
        String tradeDataLine = "20160101,13,EUR,10.0";
        StringBuilder strBuilder = new StringBuilder();

        //when
        enrichService.processSingleTradeLine(tradeDataLine, strBuilder);

        //then
        assertEquals(strBuilder.toString(), "20160101,Missing Product Name,EUR,10.0" + LINE_SEPARATOR);
    }
}
